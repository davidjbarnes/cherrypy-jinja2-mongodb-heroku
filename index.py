import cherrypy
import os

from pymongo import Connection
from bson.json_util import dumps
from bson.objectid import ObjectId
from jinja2 import Environment, FileSystemLoader

server = 'ds049150.mongolab.com'
port = 49150
db_name = 'heroku_app31123564'
username = 'test'
password = 'test'
 
conn = Connection(server, port)
db = conn[db_name]
db.authenticate(username, password)
 
env = Environment(loader=FileSystemLoader('templates'))

class Root:
    @cherrypy.expose
    def index(self):
        tmpl = env.get_template('home/index.html')
        return tmpl.render(salutation="Hello")
    
cherrypy.config.update({'server.socket_host': '0.0.0.0',})
cherrypy.config.update({'server.socket_port': int(os.environ.get('PORT', '5000')),})
cherrypy.quickstart(Root())