Basic app using Python, CherryPy web framework, Jinja2 template engine, MongoDB database, and Heroku.

Live app example URL: https://cherrypy-jinja2-mongodb-heroku.herokuapp.com/